import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { EmployeeSalary } from '../models/employeeSalary';
import { FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { EmployeeSalaryService } from '../services/employeSalary.service';
import { EmployeeSalaryComponent } from '../employee-salary.component';
import * as jspdf from 'jspdf';

@Component({
  selector: 'app-employee-salary-list',
  templateUrl: './employee-salary-list.component.html',
  styleUrls: ['./employee-salary-list.component.css']
})
export class EmployeeSalaryListComponent implements OnInit {

  isPopupOpened: true;
  salaryList : EmployeeSalary[] = [];
  searchText;
  salaryFullList : EmployeeSalary []= [];

  @ViewChild('pdfTable', {static: false}) pdfTable: ElementRef;
  constructor(public fb: FormBuilder, private dialog: MatDialog, private _employeeSalaryService: EmployeeSalaryService) {
  }

  ngOnInit() {
    this.salaryList = this._employeeSalaryService.getSalary();
    this.salaryFullList = this.salaryList;
  }

  search() {
    if (this.searchText != '') {
      this.salaryFullList = this.salaryFullList.filter(res => {
        return res.Status.toLocaleLowerCase().match(this.searchText.toLocaleLowerCase());
      });
    } else if (this.searchText == '') {
      this.ngOnInit();
    }
  }

  editSalary(id: number) {

    this.isPopupOpened = true;
    const leaveData = this._employeeSalaryService.getSalary().find(c => c.SalaryId === id);
    const dialogRef = this.dialog.open(EmployeeSalaryComponent, {
      data: leaveData
    });


    dialogRef.afterClosed().subscribe(result => {
    });
  }

  deleteSalary(id: number) {
    this._employeeSalaryService.deleteSalary(id);
  }

  addSalary() {
    const dialogConfig = new MatDialogConfig();
    this.isPopupOpened = true;
    const dialogRef = this.dialog.open(EmployeeSalaryComponent, {
      data: {}
    });


    dialogRef.afterClosed().subscribe(result => {
    });
    // dialogConfig.width = '60px';

  }


  public openPDF():void {
    let DATA = this.pdfTable.nativeElement;
    let doc = new jspdf('p','pt', 'a4');
    doc.fromHTML(DATA.innerHTML,15,15);
    doc.output('dataurlnewwindow');
  }
  
  
  public downloadPDF():void {
    let DATA = this.pdfTable.nativeElement;
    let doc = new jspdf('p','pt', 'a4');
  
    let handleElement = {
      '#editor':function(element,renderer){
        return true;
      }
    };
    doc.fromHTML(DATA.innerHTML,15,15,{
      'width': 200,
      'elementHandlers': handleElement
    });
  
    doc.save('angular-demo.pdf');
  }

}
