export interface EmployeeSalary{
    SalaryId:number,
    SalaryAmount:number,
    Tax:number,
    Status:string
}