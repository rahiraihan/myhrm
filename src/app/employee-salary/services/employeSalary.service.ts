import {Injectable} from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { EmployeeSalary } from '../models/employeeSalary';


@Injectable({
  providedIn: 'root'
})
export class EmployeeSalaryService {
  constructor(private httpClient: HttpClient, private httpService: HttpClient) {
  }

  _employeeSalaryDetails: EmployeeSalary[] = [];
  _employeeSalaryList: EmployeeSalary[] = [
    {SalaryId: 1, SalaryAmount: 100000, Tax: 10, Status: 'Paid'},
    {SalaryId: 2, SalaryAmount: 120000, Tax: 11, Status: 'UnPaid'},
    {SalaryId: 3, SalaryAmount: 130000, Tax: 12, Status: 'Paid'},
    {SalaryId: 4, SalaryAmount: 140000, Tax: 13, Status: 'UnPaid'}
   ];

  addOrUpdateSalary(empSalary: EmployeeSalary) {
    const existLeave: number = this.getSalaryId(empSalary.SalaryId);
    if (existLeave === -1) {
      //will creates leave
      empSalary.SalaryId = this._employeeSalaryList.length + 1;
    //  empSalary.EmployeeId = this.leaveDetailsList.length + 1;
      this._employeeSalaryList.push(empSalary);
    } else {
      //will updates leave
      this.deleteSalary(empSalary.SalaryId);
      this._employeeSalaryList.push(empSalary);
    }
  }

  //not used
  editSalary(empSalary: EmployeeSalary) {
    const index = this._employeeSalaryList.findIndex(c => c.SalaryId === empSalary.SalaryId);
    this._employeeSalaryList[index] = empSalary;
    this._employeeSalaryList.push(empSalary);
  }

  deleteSalary(id: number) {
    const deleteSalary: number = this.getSalaryId(id);
    this._employeeSalaryList.splice(deleteSalary, 1);
  }

  getSalary() {
    return this._employeeSalaryList;
  }

  getSalaryId(salaryId: number): number {
    return this._employeeSalaryList.findIndex(sal => sal.SalaryId === salaryId);
  }
}
