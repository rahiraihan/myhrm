import { Component, OnInit, Inject } from '@angular/core';
import { EmployeeSalaryService } from './services/employeSalary.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { EmployeeSalary } from './models/employeeSalary';

@Component({
  selector: 'app-employee-salary',
  templateUrl: './employee-salary.component.html',
  styleUrls: ['./employee-salary.component.css']
})
export class EmployeeSalaryComponent implements OnInit {

  salForm: FormGroup;

  constructor(private fb: FormBuilder, private dialog: MatDialog, private _salaryService: EmployeeSalaryService,
              @Inject(MAT_DIALOG_DATA) public data: EmployeeSalary) {
  }

  ngOnInit() {
    this.reactiveForm();
  }

  reactiveForm() {
    this.salForm = this.fb.group({
      SalaryId: [this.data.SalaryId, ''],
      SalaryAmount: [this.data.SalaryAmount, ''],
      Tax: [this.data.Tax, ''],
      Status: [this.data.Status, '']
    });
  }

  // public submitForm(): void {
  //   this.data = this.salForm.value;
  //   this._salaryService.addOrUpdateSalary(this.data);
  // }

  public confirmAdd(): void {
    this.data = this.salForm.value;
    this._salaryService.addOrUpdateSalary(this.data);
  }

}
