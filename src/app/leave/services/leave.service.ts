import {Injectable} from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {Leave} from '../models/leave';


@Injectable({
  providedIn: 'root'
})
export class LeaveService {
  constructor(private httpClient: HttpClient, private httpService: HttpClient) {
  }

  _leaveDetails: Leave[] = [];
  leaveDetailsList: Leave[] = [
    {LeaveId: 1, EmployeeId: 1, Month: 'Jan', FromDate: '11/11/2019', ToDate: '13/11/2019', NoOfDays: '3', LeavePeriod: 'a'},
    {LeaveId: 2, EmployeeId: 2, Month: 'Feb', FromDate: '10/11/2019', ToDate: '12/11/2019', NoOfDays: '3', LeavePeriod: 'b'},
    {LeaveId: 3, EmployeeId: 3, Month: 'Mar', FromDate: '09/11/2019', ToDate: '11/11/2019', NoOfDays: '3', LeavePeriod: 'ca'},
    {LeaveId: 4, EmployeeId: 4, Month: 'April', FromDate: '08/11/2019', ToDate: '10/11/2019', NoOfDays: '3', LeavePeriod: 'd'},
  ];

  addOrUpdateLeave(leave: Leave) {
    const existLeave: number = this.getLeaveId(leave.LeaveId);
    if (existLeave === -1) {
      //will creates leave
      leave.LeaveId = this.leaveDetailsList.length + 1;
      leave.EmployeeId = this.leaveDetailsList.length + 1;
      this.leaveDetailsList.push(leave);
    } else {
      //will updates leave
      this.deleteLeave(leave.LeaveId);
      this.leaveDetailsList.push(leave);
    }
  }

  //not used
  editLeave(editLeave: Leave) {
    const index = this.leaveDetailsList.findIndex(c => c.LeaveId === editLeave.LeaveId);
    this.leaveDetailsList[index] = editLeave;
    this.leaveDetailsList.push(editLeave);
  }

  deleteLeave(id: number) {
    const delLeave: number = this.getLeaveId(id);
    this.leaveDetailsList.splice(delLeave, 1);
  }

  getLeave() {
    return this.leaveDetailsList;
  }

  getLeaveId(leaveId: number): number {
    return this.leaveDetailsList.findIndex(leave => leave.LeaveId === leaveId);
  }
}
