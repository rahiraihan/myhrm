import {Component, OnInit, Inject} from '@angular/core';
import {LeaveService} from './services/leave.service';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material';
import {Leave} from './models/leave';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';

@Component({
  selector: 'app-leave',
  templateUrl: './leave.component.html',
  styleUrls: ['./leave.component.css']
})
export class LeaveComponent implements OnInit {

  leaveForm: FormGroup;

  constructor(private fb: FormBuilder, private dialog: MatDialog, private _leaveService: LeaveService,
              @Inject(MAT_DIALOG_DATA) public data: Leave) {
  }

  ngOnInit() {
    this.reactiveForm();
  }

  reactiveForm() {
    this.leaveForm = this.fb.group({
      LeaveId: [this.data.LeaveId, ''],
      Month: [this.data.Month, ''],
      EmployeeId: [this.data.EmployeeId, ''],
      FromDate: [this.data.FromDate, ''],
      ToDate: [this.data.ToDate, ''],
      LeavePeriod: [this.data.LeavePeriod, ''],
      NoOfDays: [this.data.NoOfDays, '']
    });
  }

  public submitForm(): void {
    this.data = this.leaveForm.value;
    this._leaveService.addOrUpdateLeave(this.data);
  }

  public confirmAdd(): void {
    this._leaveService.addOrUpdateLeave(this.data);
  }
}
