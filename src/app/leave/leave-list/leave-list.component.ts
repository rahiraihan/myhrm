import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {LeaveService} from '../services/leave.service';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {LeaveComponent} from '../leave.component';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Leave} from '../models/leave';
import * as jspdf from 'jspdf';

@Component({
  selector: 'app-leave-list',
  templateUrl: './leave-list.component.html',
  styleUrls: ['./leave-list.component.css']
})
export class LeaveListComponent implements OnInit {

  isPopupOpened: true;
  leaveList : Leave[] = [];
  searchText;
  leaveFullList : Leave []= [];

  @ViewChild('pdfTable', {static: false}) pdfTable: ElementRef;
  constructor(public fb: FormBuilder, private dialog: MatDialog, private _leaveService: LeaveService) {
  }

  ngOnInit() {
    this.leaveList = this._leaveService.getLeave();
    this.leaveFullList = this.leaveList;
  }

  search() {
    if (this.searchText != '') {
      this.leaveFullList = this.leaveFullList.filter(res => {
        return res.Month.toLocaleLowerCase().match(this.searchText.toLocaleLowerCase());
      });
    } else if (this.searchText == '') {
      this.ngOnInit();
    }
  }

  editLeave(id: number) {

    this.isPopupOpened = true;
    const leaveData = this._leaveService.getLeave().find(c => c.LeaveId === id);
    const dialogRef = this.dialog.open(LeaveComponent, {
      data: leaveData
    });


    dialogRef.afterClosed().subscribe(result => {
    });
  }

  deleteLeave(id: number) {
    this._leaveService.deleteLeave(id);
  }

  addLeave() {
    const dialogConfig = new MatDialogConfig();
    this.isPopupOpened = true;
    const dialogRef = this.dialog.open(LeaveComponent, {
      data: {}
    });


    dialogRef.afterClosed().subscribe(result => {
    });
    // dialogConfig.width = '60px';

  }
  public openPDF():void {
    let DATA = this.pdfTable.nativeElement;
    let doc = new jspdf('p','pt', 'a4');
    doc.fromHTML(DATA.innerHTML,15,15);
    doc.output('dataurlnewwindow');
  }
  
  
  public downloadPDF():void {
    let DATA = this.pdfTable.nativeElement;
    let doc = new jspdf('p','pt', 'a4');
  
    let handleElement = {
      '#editor':function(element,renderer){
        return true;
      }
    };
    doc.fromHTML(DATA.innerHTML,15,15,{
      'width': 200,
      'elementHandlers': handleElement
    });
  
    doc.save('angular-demo.pdf');
  }
}
