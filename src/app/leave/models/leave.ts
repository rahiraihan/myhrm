export interface Leave{
    
    LeaveId: number;
    EmployeeId: number; 
    Month: string;
    FromDate: string; 
    ToDate: string ; 
    NoOfDays :string; 
    LeavePeriod: string;
}