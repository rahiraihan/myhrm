import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { HrmLogoutComponent } from '../hrm-login/hrm-logout/hrm-logout.component';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  // @Input() public isUserLoggedIn: boolean;
  isLoggedIn: boolean;
  isPopupOpened: true;
  constructor(private router: Router, private authService : AuthService, private dialog: MatDialog) { }

  ngOnInit() {
  }
  logOut() {
    this.authService.logout()
    this.router.navigate(['login']);
  }
  openLogout() {
    const dialogConfig = new MatDialogConfig();
    this.isPopupOpened = true;
    const dialogRef = this.dialog.open(HrmLogoutComponent, {
      data: {}
    });
    dialogRef.afterClosed().subscribe(result => {
    });
    // dialogConfig.width = '60px';

  }
}
