import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-location',
  templateUrl: './user-location.component.html',
  styleUrls: ['./user-location.component.css']
})
export class UserLocationComponent implements OnInit {
  
  lat: number = 23.7272;
  lng: number = 90.4093;
  location: object;
  userList: any =[];
  constructor(private _userService: UserService) { }

  ngOnInit() {
    this._userService.getLocation().subscribe(data => {
      console.log(data);
      this.lat = this.userList;
      this.lng = this.userList;
    })
  }

getData($event){
  this.userList = $event;
}

}
