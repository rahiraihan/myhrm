import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog, MatDialogConfig  } from '@angular/material';
import { UserService } from '../services/user.service';
import { UserComponent } from '../user.component';
import { FormsModule, FormArray, FormControl, FormGroup }   from '@angular/forms';
import * as jspdf from 'jspdf';  
import html2canvas from 'html2canvas'; 



@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  
  isChecked:boolean  = false;
  form:FormGroup;
  masterSelected:boolean;
  checklist:any;
  checkedList:any;
  exampleParent:  string;
  getUserList: any=[];
  isPopupOpened: true;
  dataSource = new MatTableDataSource(this._userService._userDetail);
  @ViewChild('pdfTable', {static: false}) userLists: ElementRef;

  @Output() userList = new EventEmitter<string>();
  constructor(private dialog: MatDialog , private _userService: UserService) {
    this.masterSelected = false;
   }

  ngOnInit() {
   this.getUserList = this._userService.getUserDetail();
   this.checklist = this.getUserList;
  }
  // get userList() {
  //   return this._userService.getUserDetail();
  // }
 
  //pdfSrc = "http://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf";

  sendMapData(){
    if (this.isChecked == true){
      this.sendData();
    }
    else {
      alert("Danger");
    }
  }

  onCheckboxChange(e) {
    const isSelect: FormArray = this.form.get('isSelect') as FormArray;
  
    if (e.target.checked) {
      isSelect.push(new FormControl(e.target.value));
    } else {
       const index = isSelect.controls.findIndex(x => x.value === e.target.value);
       isSelect.removeAt(index);
    }
  }
  
  sendData(){
    this.userList.emit(this.getUserList);
    console.log("Clicked");
  }
  addUser() {
    const dialogConfig = new MatDialogConfig();
    this.isPopupOpened = true;
    const dialogRef = this.dialog.open(UserComponent, {
      data: {}
    });


    dialogRef.afterClosed().subscribe(result => {
    });
  //  dialogConfig.width = '60px';
   
  }

  editUser(id: number) {
    this.isPopupOpened = true;
    const usersList = this._userService.getUserDetail().find(c => c.UserId === id);
    const dialogRef = this.dialog.open(UserComponent, {
      data: {usersList}
    });


    dialogRef.afterClosed().subscribe(result => {
     data: usersList
    });
  }

  deleteUser(id: number) {
    this._userService.deleteUserDetail(id);
  }
 
  parentMethod($event){
  this.exampleParent = $event;
  }
  


  checkUncheckAll() {
    for (var i = 0; i < this.checklist.length; i++) {
      this.checklist[i].isSelected = this.masterSelected;
    }
    this.getCheckedItemList();
  }
  isAllSelected() {
    this.masterSelected = this.checklist.every(function(item:any) {
        return item.isSelected == true;
      })
    this.getCheckedItemList();
  }

  getCheckedItemList(){
    this.checkedList = [];
    for (var i = 0; i < this.checklist.length; i++) {
      if(this.checklist[i].isSelected)
      this.checkedList.push(this.checklist[i]);
    }
    this.checkedList = JSON.stringify(this.checkedList);
  }

  public captureScreen()  
  {  
    var data = document.getElementById('contentToConvert');  
    html2canvas(data).then(canvas => {  
      // Few necessary setting options  
      var imgWidth = 208;   
      var pageHeight = 295;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      var heightLeft = imgHeight;  

      const contentDataURL = canvas.toDataURL('image/png')  
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;  
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
      pdf.save('MYPdf.pdf'); // Generated PDF   
    });  
  } 
}
