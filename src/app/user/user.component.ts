import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder} from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { UserService } from './services/user.service';
import { User } from './models/user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  
  userForm: FormGroup;
  constructor(private fb: FormBuilder,private dialog: MatDialog , private _userService: UserService,
    @Inject(MAT_DIALOG_DATA) public data: User,){

  }
 
  ngOnInit() {
    this.reactiveForm();
  }
  reactiveForm() {
    this.userForm = this.fb.group({
      UserId: [this.data.UserId, ''],
      UserName: [this.data.UserName, ''],
      Password: [this.data.Password, ''],
      UserGender: [this.data.UserGender, ''],
      Role: [this.data.Role, ''],
      Token: [this.data.Token, ''],
      Attendance: [this.data.Attendance, ''],
      IpAddress: [this.data.IpAddress, '']
    });
  }
  
  public confirmAdd(): void {
    this.data = this.userForm.value;
    this._userService.addOrUpdateUser(this.data);
  }
}
