export interface User{
    
    UserId: number;
    UserName: string; 
    Password:  string;
    UserGender: string; 
    Role: string ; 
    Token:string; 
    Attendance: number;
    IpAddress: string;
    latS:any;
    lngS:any;
    isSelected:boolean
}