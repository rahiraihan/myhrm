import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';
import { User } from '../models/user';


interface Location{
  longitude:string;
  latitude:string;
}

@Injectable({
  providedIn: 'root'
})
export class UserService{
  constructor(private httpClient: HttpClient, private httpService: HttpClient) { }
 
 _userDetails: User[] = [];
 _userDetail: User[] = [
    {UserId: 1, UserName: 'admin', Password: 'admin', UserGender: 'Male',Role:'Admin',Token:'asd55689654',Attendance: 1,IpAddress:'172.72.82.0',latS:23.7272,lngS:90.4039,isSelected:false},
    {UserId: 2, UserName: 'Maria', Password: 'maria', UserGender: 'Female',Role:'User',Token:'asd588989654',Attendance: 2,IpAddress:'172.82.92.0',latS:23.7272,lngS:90.4039,isSelected:false},
    {UserId: 3, UserName: 'Shaon', Password: 'shaon', UserGender: 'Male',Role:'User',Token:'asd567889654',Attendance: 5,IpAddress:'164.92.81.1',latS:23.7272,lngS:90.4039,isSelected:false},
    {UserId: 4, UserName: 'Tara', Password: 'tara', UserGender: 'Female',Role:'User',Token:'asd5559589654',Attendance: 6,IpAddress:'172.72.82.0',latS:23.7272,lngS:90.4039,isSelected:false},
    {UserId: 5, UserName: 'Rahi', Password: 'rahi', UserGender: 'Male',Role:'User',Token:'asd589654',Attendance: 3,IpAddress:'172.72.82.0',latS:23.7272,lngS:90.4039,isSelected:false},
    {UserId: 6, UserName: 'Kriti', Password: 'kriti', UserGender: 'Female',Role:'User',Token:'asd589654',Attendance: 3,IpAddress:'172.72.82.0',latS:23.7272,lngS:90.4039,isSelected:false},
    {UserId: 7, UserName: 'Raman', Password: 'raman', UserGender: 'Male',Role:'User',Token:'asdasd589654',Attendance: 2,IpAddress:'172.72.82.0',latS:23.7272,lngS:90.4039,isSelected:false},
    {UserId: 8, UserName: 'Rashi', Password: 'rashi', UserGender: 'Female',Role:'User',Token:'asda245sd589654',Attendance: 10,IpAddress:'172.72.82.0',latS:23.7272,lngS:90.4039,isSelected:false},
    {UserId: 9, UserName: 'Kiren', Password: 'kiren', UserGender: 'Male',Role:'User',Token:'asd578asd589654',Attendance: 11,IpAddress:'172.72.82.0',latS:23.7272,lngS:90.4039,isSelected:false},
    
  ];
  addUserDetail(addUser: User) {
    addUser.UserId = this._userDetail.length + 1;
    this._userDetail.push(addUser);
  }
  getLocation(){

    return this.httpClient.get<Location>('');
  }
  addOrUpdateUser(addUser: User) {
    const users: number = this.getUserId(addUser.UserId);
    if (users === -1) {
      //will creates leave
      addUser.UserId = this._userDetail.length + 1;
     // addUser.EmployeeId = this._userDetail.length + 1;
      this._userDetail.push(addUser);
    } else {
      //will updates leave
      this.deleteUserDetail(addUser.UserId);
      this._userDetail.push(addUser);
    }
  }
  editUserDetail(addUser: User) {
    const index = this._userDetail.findIndex(c => c.UserId === addUser.UserId);
    this._userDetail[index] = addUser;
    this._userDetail.push(addUser);
  }

  getUserDetail() {
    return this._userDetail;
  }

  deleteUserDetail(id: number) {
    const delUser = this.getUserId(id);
    this._userDetail.splice(delUser, 1);
  }

 
  getUserId(userId: number): number {
    return this._userDetail.findIndex(x => x.UserId === userId);
  }
}