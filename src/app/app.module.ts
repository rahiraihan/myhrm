import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatSortModule} from '@angular/material/sort';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NavbarComponent} from './navbar/navbar.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import { PdfViewerModule } from "ng2-pdf-viewer";
import {RouterModule} from '@angular/router';
import {HrmLoginComponent} from './hrm-login/hrm-login.component';
import {HrmAttendanceListComponent} from './hrm-login/hrm-attendance-list/hrm-attendance-list.component';
import {UserComponent} from './user/user.component';
import {UserListComponent} from './user/user-list/user-list.component';
import {LeaveComponent} from './leave/leave.component';
import {LeaveListComponent} from './leave/leave-list/leave-list.component';
import {PayrollComponent} from './payroll/payroll.component';
import {PayrollListComponent} from './payroll/payroll-list/payroll-list.component';
import {EmployeeComponent} from './employee/employee.component';
import {EmployeeListComponent} from './employee/employee-list/employee-list.component';
import {LoanComponent} from './loan/loan.component';
import {LoanListComponent} from './loan/loan-list/loan-list.component';
import {TrainingComponent} from './training/training.component';
import {TrainingListComponent} from './training/training-list/training-list.component';
import {AgmCoreModule} from '@agm/core';
import {UserLocationComponent} from './user/user-location/user-location.component';
import {AuthGuardService} from './auth/auth-guard.service';
import {MatCardModule} from '@angular/material';
import { DepartmentComponent } from './department/department.component';
import { DepartmentListComponent } from './department/department-list/department-list.component';
import { EmployeeSalaryComponent } from './employee-salary/employee-salary.component';
import { EmployeeSalaryListComponent } from './employee-salary/employee-salary-list/employee-salary-list.component';
import { EmployeeProjectComponent } from './employee-project/employee-project.component';
import { EmployeeProjectListComponent } from './employee-project/employee-project-list/employee-project-list.component';
import { EmployeeAttendanceComponent } from './employee-attendance/employee-attendance.component';
import { EmployeeAttendanceListComponent } from './employee-attendance/employee-attendance-list/employee-attendance-list.component';
import { ButtonsModule, WavesModule, CollapseModule } from 'angular-bootstrap-md'
import {DragDropModule} from '@angular/cdk/drag-drop';
import { HrmLogoutComponent } from './hrm-login/hrm-logout/hrm-logout.component';
//import { Ng2SearchPipeModule } from '@ng2-search-filter';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DashboardComponent,
    HrmLoginComponent,
    HrmAttendanceListComponent,
    UserComponent,
    UserListComponent,
    LeaveComponent,
    LeaveListComponent,
    PayrollComponent,
    PayrollListComponent,
    EmployeeComponent,
    EmployeeListComponent,
    LoanComponent,
    LoanListComponent,
    TrainingComponent,
    TrainingListComponent,
    UserLocationComponent,
    DepartmentComponent,
    DepartmentListComponent,
    EmployeeSalaryComponent,
    EmployeeSalaryListComponent,
    EmployeeProjectComponent,
    EmployeeProjectListComponent,
    EmployeeAttendanceComponent,
    EmployeeAttendanceListComponent,
    HrmLogoutComponent
    // FormsModule,
    // ReactiveFormsModule
  ],
    imports: [
        ReactiveFormsModule,
        HttpClientModule,
        BrowserModule,
        DragDropModule,
        CommonModule,
        MatToolbarModule,
        MatGridListModule,
        MatFormFieldModule,
        MatInputModule,
        MatRadioModule,
        MatSelectModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatButtonModule,
        MatSnackBarModule,
        MatTableModule,
        MatIconModule,
        MatSortModule,
        MatDialogModule,
        ReactiveFormsModule,
        FormsModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        PdfViewerModule,
        ButtonsModule, 
        WavesModule,
        CollapseModule,
        // Ng2SearchPipeModule,
        RouterModule.forRoot([

            {path: 'navbar', component: NavbarComponent},
            {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService]},
            {path: 'user', component: UserComponent, canActivate: [AuthGuardService]},
            {path: 'user-list', component: UserListComponent, canActivate: [AuthGuardService]},
            {path: 'login', component: HrmLoginComponent},
            {path: 'attend', component: HrmAttendanceListComponent, canActivate: [AuthGuardService]},
            {path: 'leave', component: LeaveComponent, canActivate: [AuthGuardService]},
            {path: 'leave-list', component: LeaveListComponent, canActivate: [AuthGuardService]},
            {path: 'payroll', component: PayrollComponent, canActivate: [AuthGuardService]},
            {path: 'payroll-list', component: PayrollListComponent, canActivate: [AuthGuardService]},
            {path: 'employee', component: EmployeeComponent, canActivate: [AuthGuardService]},
            {path: 'employee-list', component: EmployeeListComponent, canActivate: [AuthGuardService]},
            {path: 'loan', component: LoanComponent, canActivate: [AuthGuardService]},
            {path: 'loan-list', component: LoanListComponent, canActivate: [AuthGuardService]},
            {path: 'training', component: TrainingComponent, canActivate: [AuthGuardService]},
            {path: 'training-list', component: TrainingListComponent, canActivate: [AuthGuardService]},
            {path: 'user-location', component: UserLocationComponent, canActivate: [AuthGuardService]},
            {path: 'department', component: DepartmentComponent, canActivate: [AuthGuardService]},
            {path: 'department-list', component: DepartmentListComponent, canActivate: [AuthGuardService]},
            {path: 'employee-salary', component: EmployeeSalaryComponent, canActivate: [AuthGuardService]},
            {path: 'employee-salary-list', component: EmployeeSalaryListComponent, canActivate: [AuthGuardService]},
            {path: 'employee-attendance', component: EmployeeAttendanceComponent, canActivate: [AuthGuardService]},
            {path: 'employee-attendance-list', component: EmployeeAttendanceListComponent, canActivate: [AuthGuardService]},
            {path: 'employee-project', component: EmployeeProjectComponent, canActivate: [AuthGuardService]},
            {path: 'employee-project-list', component: EmployeeProjectListComponent, canActivate: [AuthGuardService]},
            {path: '**', redirectTo: '/'}

        ]),
        AgmCoreModule.forRoot({
            apiKey: ''
        }),
        MatCardModule


    ],
  entryComponents: [NavbarComponent,HrmLogoutComponent],
  exports: [
    // MatToolbarModule,
    // MatGridListModule,
    // MatFormFieldModule,
    // MatInputModule,
    // MatRadioModule,
    // MatSelectModule,
    // MatCheckboxModule,
    // MatDatepickerModule,
    // MatNativeDateModule,
    // MatButtonModule,
    // MatSnackBarModule,
    // MatTableModule,
    // MatIconModule,
    // MatSortModule,
    // MatDialogModule,

  ],
  providers: [],

  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
