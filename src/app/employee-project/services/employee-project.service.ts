import {Injectable} from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { EmployeeProject } from '../models/employeeProject';


@Injectable({
  providedIn: 'root'
})
export class EmployeeProjectService {
  constructor(private httpClient: HttpClient, private httpService: HttpClient) {
  }

  _employeeProjectDetails: EmployeeProject[] = [];
  _employeeProjectDetail: EmployeeProject[] = [
    {ProjectHandleId: 1, ProjectName: "HRM", DateStarted: "Jan", DateEnded: '11/11/2019', Status: 'On Going'},
    {ProjectHandleId: 2, ProjectName: "ERP", DateStarted: "Jan", DateEnded: '11/11/2019', Status: 'On Going'},
    {ProjectHandleId: 3, ProjectName: "LMS", DateStarted: "Jan", DateEnded: '11/11/2019', Status: 'On Going'},
    {ProjectHandleId: 4, ProjectName: "UMS", DateStarted: "Jan", DateEnded: '11/11/2019', Status: 'On Going'},
    {ProjectHandleId: 5, ProjectName: "IMS", DateStarted: "Jan", DateEnded: '11/11/2019', Status: 'On Going'},
];

  addOrUpdateProject(empProject: EmployeeProject) {
    const existPro: number = this.getProjectById(empProject.ProjectHandleId);
    if (existPro === -1) {
      //will creates leave
      empProject.ProjectHandleId = this._employeeProjectDetail.length + 1;
      //empProject.EmployeeId = this._employeeProjectDetail.length + 1;
      this._employeeProjectDetail.push(empProject);
    } else {
      //will updates leave
      this.deleteProject(empProject.ProjectHandleId);
      this._employeeProjectDetail.push(empProject);
    }
  }

  //not used
  editProject(empProject: EmployeeProject) {
    const index = this._employeeProjectDetail.findIndex(c => c.ProjectHandleId === empProject.ProjectHandleId);
    this._employeeProjectDetail[index] = empProject;
    this._employeeProjectDetail.push(empProject);
  }

  deleteProject(id: number) {
    const delLeave: number = this.getProjectById(id);
    this._employeeProjectDetail.splice(delLeave, 1);
  }

  getProject() {
    return this._employeeProjectDetail;
  }

  getProjectById(projectHandleId: number): number {
    return this._employeeProjectDetail.findIndex(x => x.ProjectHandleId === projectHandleId);
  }
}
