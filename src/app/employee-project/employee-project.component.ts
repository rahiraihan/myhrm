import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { EmployeeProjectService } from './services/employee-project.service';
import { EmployeeProject } from './models/employeeProject';

@Component({
  selector: 'app-employee-project',
  templateUrl: './employee-project.component.html',
  styleUrls: ['./employee-project.component.css']
})
export class EmployeeProjectComponent implements OnInit {

  proForm: FormGroup;

  constructor(private fb: FormBuilder, private dialog: MatDialog, private _employeeService: EmployeeProjectService,
              @Inject(MAT_DIALOG_DATA) public data: EmployeeProject) {
  }

  ngOnInit() {
    this.reactiveForm();
  }

  reactiveForm() {
    this.proForm = this.fb.group({
      ProjectHandleId: [this.data.ProjectHandleId, ''],
      ProjectName: [this.data.ProjectName, ''],
      DateStarted: [this.data.DateStarted, ''],
      DateEnded: [this.data.DateEnded, ''],
      Status: [this.data.Status, '']
    });
  }

  // public submitForm(): void {
  //   this.data = this.salForm.value;
  //   this._salaryService.addOrUpdateSalary(this.data);
  // }

  public confirmAdd(): void {
    this.data = this.proForm.value;
    this._employeeService.addOrUpdateProject(this.data);
  }

}
