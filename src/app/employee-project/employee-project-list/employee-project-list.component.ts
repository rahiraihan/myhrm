import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { EmployeeProject } from '../models/employeeProject';
import { FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { EmployeeProjectService } from '../services/employee-project.service';
import { EmployeeProjectComponent } from '../employee-project.component';
import * as jspdf from 'jspdf';

@Component({
  selector: 'app-employee-project-list',
  templateUrl: './employee-project-list.component.html',
  styleUrls: ['./employee-project-list.component.css']
})
export class EmployeeProjectListComponent implements OnInit {

  isPopupOpened: true;
  projectList : EmployeeProject[] = [];
  searchText;
  projectFullList : EmployeeProject []= [];

  @ViewChild('pdfTable', {static: false}) pdfTable: ElementRef;
  constructor(public fb: FormBuilder, private dialog: MatDialog, private _employeeProService: EmployeeProjectService) {
  }

  ngOnInit() {
    this.projectList = this._employeeProService.getProject();
    this.projectFullList = this.projectList;
  }

  search() {
    if (this.searchText != '') {
      this.projectFullList = this.projectFullList.filter(res => {
        return res.ProjectName.toLocaleLowerCase().match(this.searchText.toLocaleLowerCase());
      });
    } else if (this.searchText == '') {
      this.ngOnInit();
    }
  }

  editProject(id: number) {

    this.isPopupOpened = true;
    const atData = this._employeeProService.getProject().find(c => c.ProjectHandleId === id);
    const dialogRef = this.dialog.open(EmployeeProjectComponent, {
      data: atData
    });


    dialogRef.afterClosed().subscribe(result => {
    });
  }

  deleteProject(id: number) {
    this._employeeProService.deleteProject(id);
  }

  addProject() {
    const dialogConfig = new MatDialogConfig();
    this.isPopupOpened = true;
    const dialogRef = this.dialog.open(EmployeeProjectComponent, {
      data: {}
    });


    dialogRef.afterClosed().subscribe(result => {
    });
    // dialogConfig.width = '60px';

  }
  
  public openPDF():void {
    let DATA = this.pdfTable.nativeElement;
    let doc = new jspdf('p','pt', 'a4');
    doc.fromHTML(DATA.innerHTML,15,15);
    doc.output('dataurlnewwindow');
  }
  
  
  public downloadPDF():void {
    let DATA = this.pdfTable.nativeElement;
    let doc = new jspdf('p','pt', 'a4');
  
    let handleElement = {
      '#editor':function(element,renderer){
        return true;
      }
    };
    doc.fromHTML(DATA.innerHTML,15,15,{
      'width': 200,
      'elementHandlers': handleElement
    });
  
    doc.save('angular-demo.pdf');
  }
}
