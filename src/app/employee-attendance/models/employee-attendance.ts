export interface EmployeeAttendance{
    EmployeeId:number,
    TimeIn:string,
    TimeOut:string,
    Remarks:string,
    }