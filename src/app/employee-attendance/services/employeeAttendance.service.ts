import {Injectable} from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { EmployeeAttendance } from '../models/employee-attendance';


@Injectable({
  providedIn: 'root'
})
export class EmployeeAttendanceService {
  constructor(private httpClient: HttpClient, private httpService: HttpClient) {
  }

  _employeeAttendDetails: EmployeeAttendance[] = [];
  _employeeAttendDetail: EmployeeAttendance[] = [
    {EmployeeId: 1, TimeIn: '10:00', TimeOut: '05:00', Remarks: 'Present'},
    {EmployeeId: 2, TimeIn: 'Null', TimeOut: 'Null', Remarks: 'Absent'},
    {EmployeeId: 3, TimeIn: '10:00', TimeOut: '06:00', Remarks: 'Present'},
    {EmployeeId: 4, TimeIn: 'Null', TimeOut: 'Null', Remarks: 'Absent'},
   ];

  addOrUpdateEmployeeAttendance(empAttend: EmployeeAttendance) {
    const existAttend: number = this.getAttendanceId(empAttend.EmployeeId);
    if (existAttend === -1) {
      //will creates leave
      empAttend.EmployeeId = this._employeeAttendDetail.length + 1;
    //  empSalary.EmployeeId = this.leaveDetailsList.length + 1;
      this._employeeAttendDetail.push(empAttend);
    } else {
      //will updates leave
      this.deleteAttendance(empAttend.EmployeeId);
      this._employeeAttendDetail.push(empAttend);
    }
  }

  //not used
  editSalary(empAttend: EmployeeAttendance) {
    const index = this._employeeAttendDetail.findIndex(c => c.EmployeeId === empAttend.EmployeeId);
    this._employeeAttendDetail[index] = empAttend;
    this._employeeAttendDetail.push(empAttend);
  }

  deleteAttendance(id: number) {
    const deleteAttendance: number = this.getAttendanceId(id);
    this._employeeAttendDetail.splice(deleteAttendance, 1);
  }

  getAttendance() {
    return this._employeeAttendDetail;
  }

  getAttendanceId(employeeId: number): number {
    return this._employeeAttendDetail.findIndex(sal => sal.EmployeeId === employeeId);
  }
}
