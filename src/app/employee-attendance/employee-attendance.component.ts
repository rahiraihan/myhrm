import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { EmployeeAttendanceService } from './services/employeeAttendance.service';
import { EmployeeAttendance } from './models/employee-attendance';

@Component({
  selector: 'app-employee-attendance',
  templateUrl: './employee-attendance.component.html',
  styleUrls: ['./employee-attendance.component.css']
})
export class EmployeeAttendanceComponent implements OnInit {

  attendForm: FormGroup;

  constructor(private fb: FormBuilder, private dialog: MatDialog, private _employeeService: EmployeeAttendanceService,
              @Inject(MAT_DIALOG_DATA) public data: EmployeeAttendance) {
  }

  ngOnInit() {
    this.reactiveForm();
  }

  reactiveForm() {
    this.attendForm = this.fb.group({
      EmployeeId: [this.data.EmployeeId, ''],
      TimeIn: [this.data.TimeIn, ''],
      TimeOut: [this.data.TimeOut, ''],
      Remarks: [this.data.Remarks, '']
    });
  }

  // public submitForm(): void {
  //   this.data = this.salForm.value;
  //   this._salaryService.addOrUpdateSalary(this.data);
  // }

  public confirmAdd(): void {
    this.data = this.attendForm.value;
    this._employeeService.addOrUpdateEmployeeAttendance(this.data);
  }

}
