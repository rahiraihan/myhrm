import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { EmployeeAttendance } from '../models/employee-attendance';
import { FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { EmployeeAttendanceService } from '../services/employeeAttendance.service';
import { EmployeeAttendanceComponent } from '../employee-attendance.component';
import * as jspdf from 'jspdf';

@Component({
  selector: 'app-employee-attendance-list',
  templateUrl: './employee-attendance-list.component.html',
  styleUrls: ['./employee-attendance-list.component.css']
})
export class EmployeeAttendanceListComponent implements OnInit {

  isPopupOpened: true;
  attendList : EmployeeAttendance[] = [];
  searchText;
  attendFullList : EmployeeAttendance []= [];

  @ViewChild('pdfTable', {static: false}) pdfTable: ElementRef;
  constructor(public fb: FormBuilder, private dialog: MatDialog, private _employeeAttendService: EmployeeAttendanceService) {
  }

  ngOnInit() {
    this.attendList = this._employeeAttendService.getAttendance();
    this.attendFullList = this.attendList;
  }

  search() {
    if (this.searchText != '') {
      this.attendFullList = this.attendFullList.filter(res => {
        return res.Remarks.toLocaleLowerCase().match(this.searchText.toLocaleLowerCase());
      });
    } else if (this.searchText == '') {
      this.ngOnInit();
    }
  }

  editAttendance(id: number) {

    this.isPopupOpened = true;
    const atData = this._employeeAttendService.getAttendance().find(c => c.EmployeeId === id);
    const dialogRef = this.dialog.open(EmployeeAttendanceComponent, {
      data: atData
    });


    dialogRef.afterClosed().subscribe(result => {
    });
  }

  deleteAttendance(id: number) {
    this._employeeAttendService.deleteAttendance(id);
  }

  addAttendance() {
    const dialogConfig = new MatDialogConfig();
    this.isPopupOpened = true;
    const dialogRef = this.dialog.open(EmployeeAttendanceComponent, {
      data: {}
    });


    dialogRef.afterClosed().subscribe(result => {
    });
    // dialogConfig.width = '60px';

  }

  public openPDF():void {
    let DATA = this.pdfTable.nativeElement;
    let doc = new jspdf('p','pt', 'a4');
    doc.fromHTML(DATA.innerHTML,15,15);
    doc.output('dataurlnewwindow');
  }
  
  
  public downloadPDF():void {
    let DATA = this.pdfTable.nativeElement;
    let doc = new jspdf('p','pt', 'a4');
  
    let handleElement = {
      '#editor':function(element,renderer){
        return true;
      }
    };
    doc.fromHTML(DATA.innerHTML,15,15,{
      'width': 200,
      'elementHandlers': handleElement
    });
  
    doc.save('angular-demo.pdf');
  }
}
