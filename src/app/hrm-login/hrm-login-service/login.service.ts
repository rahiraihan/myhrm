import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';

@Injectable()
export class LoginService {
 
 constructor(private router: Router, private authService: AuthService) {}

  logout() {
    this.authService.logout()
    this.router.navigate(['/login']);
  }
}