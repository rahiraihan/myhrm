import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material'
import { UserService } from '../user/services/user.service';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-hrm-login',
  templateUrl: './hrm-login.component.html',
  styleUrls: ['./hrm-login.component.css']
})
export class HrmLoginComponent implements OnInit {

constructor(private router: Router, private _user: UserService , private authService :AuthService) { }
USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'
username: string;
password: string;
userInitials: any [] = [];
attendance: string = 'You Are Attended Today';
@Output() valueChange = new EventEmitter();
  
ngOnInit() {
  //this.userInitials = this._user._userDetails.values.name(this.username,this.password);
}

//   valueChanged() { 
  
// }
  login() : void {
    if(this.username == 'admin' && this.password == 'admin') {
      this.valueChange.emit(this.attendance);
      this.authService.setUsernameInSession(this.username)
      this.router.navigate(["dashboard"]);
      console.log(this.attendance);
     
    }else {
      alert("Invalid credentials");
    }
  }
}

