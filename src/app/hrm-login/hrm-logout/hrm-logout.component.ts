import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';


@Component({
  selector: 'app-hrm-logout',
  templateUrl: './hrm-logout.component.html',
  styleUrls: ['./hrm-logout.component.css']
})
export class HrmLogoutComponent implements OnInit {

  constructor (private router: Router, private authService : AuthService, private dialogRef: MatDialogRef<HrmLogoutComponent>) { }

  ngOnInit() {
  }
  logOutSystem() {
    this.authService.logout()
    this.router.navigate(['login']);
    this.dialogRef.close();
  }
  closeModal() {
    this.dialogRef.close();
  }
}
