import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';
import { Department } from '../model/department';




@Injectable({
  providedIn: 'root'
})
export class DepartmentService{
  constructor(private httpClient: HttpClient, private httpService: HttpClient) { }
 
  _departmentDetails: Department[] = [];
  _departmentDetail: Department[] = [
    { DepartmentId: 1, DepartmentName: 'HRM',},
    { DepartmentId: 2, DepartmentName: 'CSE',},
    { DepartmentId: 3, DepartmentName: 'EEE',},
    { DepartmentId: 4, DepartmentName: 'IPE',},
     ];
    addDepartment(addDepartment: Department) {
    addDepartment.DepartmentId = this._departmentDetail.length + 1;
    this._departmentDetail.push(addDepartment);
  }

  editDepartment(editDepartment: Department) {
    const index = this._departmentDetail.findIndex(c => c.DepartmentId === editDepartment.DepartmentId);
    this._departmentDetail[index] = editDepartment;
    this._departmentDetail.push(editDepartment);
  }
  addOrUpdateDepartment(dept: Department) {
    const existDepartment: number = this.getDepartmentId(dept.DepartmentId);
    if (existDepartment === -1) {
      //will creates leave
      dept.DepartmentId = this._departmentDetail.length + 1;
      //dept.DepartmentId = this._departmentDetail.length + 1;
      this._departmentDetail.push(dept);
    } else {
      //will updates leave
      this.deleteDepartment(dept.DepartmentId);
      this._departmentDetail.push(dept);
    }
  }

  deleteDepartment(id: number) {
    const delDepartment = this.getDepartmentId(id);
    this._departmentDetail.splice(delDepartment, 1);
  }

  getDepartment() {
    return this._departmentDetail;
  }
  getDepartmentId(departmentId: number): number {
    return this._departmentDetail.findIndex(x => x.DepartmentId === departmentId);
  }
}