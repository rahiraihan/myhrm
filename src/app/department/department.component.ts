import { Component, OnInit, Inject } from '@angular/core';
import { DepartmentService } from './services/department.service';
import { Department } from './model/department';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {
  deptForm: FormGroup;
  constructor(private fb: FormBuilder, private dialog: MatDialog , private _depService: DepartmentService,
    @Inject(MAT_DIALOG_DATA) public data: Department) { }

  ngOnInit() {
    this.reactiveForm();
  }
  reactiveForm() {
    this.deptForm = this.fb.group({
      DepartmentId: [this.data.DepartmentId, ''],
      DepartmentName: [this.data.DepartmentName, '']
    });
  }
  public confirmAdd(): void {
    this.data = this.deptForm.value;
    this._depService.addOrUpdateDepartment(this.data);
  }
}
