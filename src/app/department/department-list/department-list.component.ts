import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Department } from '../model/department';
import { DepartmentService } from '../services/department.service';
import { MatDialog, MatTableDataSource, MatDialogConfig } from '@angular/material';
import { DepartmentComponent } from '../department.component';
import * as jspdf from 'jspdf';  
@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.css']
})
export class DepartmentListComponent implements OnInit {
  isPopupOpened: true;
  departmentList:Department [] = [];
  searchText;
  departmentFullList:Department [] =  [];
  @ViewChild('pdfTable', {static: false}) pdfTable: ElementRef;
 
  constructor(private dialog: MatDialog , private _departmentService: DepartmentService) { }

  ngOnInit() {
    this.departmentList = this._departmentService.getDepartment();
    this.departmentFullList = this.departmentList;
  }
  search(){
    if (this.searchText !=""){
      this.departmentFullList = this.departmentFullList.filter(res =>{ 
        return res.DepartmentName.toLocaleLowerCase().match(this.searchText.toLocaleLowerCase())
       });
  } else if (this.searchText ==""){
    this.ngOnInit();
    }
  }

  addDepartment() {
    const dialogConfig = new MatDialogConfig();
    this.isPopupOpened = true;
    const dialogRef = this.dialog.open(DepartmentComponent, {
      data: {}
    });
    dialogRef.afterClosed().subscribe(result => {
    });
    //dialogConfig.width = '60px';
   
  }

  editDepartment(id: number) {
    this.isPopupOpened = true;
    const editDepartment = this._departmentService.getDepartment().find(c => c.DepartmentId === id);
    const dialogRef = this.dialog.open(DepartmentComponent, {
      data: editDepartment
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  deleteDepartment(id: number) {
    this._departmentService.deleteDepartment(id);
  }

  

  public openPDF():void {
    let DATA = this.pdfTable.nativeElement;
    let doc = new jspdf('p','pt', 'a4');
    doc.fromHTML(DATA.innerHTML,15,15);
    doc.output('dataurlnewwindow');
  }


  public downloadPDF():void {
    let DATA = this.pdfTable.nativeElement;
    let doc = new jspdf('p','pt', 'a4');

    let handleElement = {
      '#editor':function(element,renderer){
        return true;
      }
    };
    doc.fromHTML(DATA.innerHTML,15,15,{
      'width': 200,
      'elementHandlers': handleElement
    });

    doc.save('angular-demo.pdf');
  }
}
