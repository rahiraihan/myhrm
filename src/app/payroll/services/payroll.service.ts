import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';
import { Payroll } from '../models/payroll';




@Injectable({
  providedIn: 'root'
})
export class PayrollService{
  constructor(private httpClient: HttpClient, private httpService: HttpClient) { }
 
  _payrollDetails: Payroll[] = [];
  _payrollDetail: Payroll[] = [
    { PayrollId: 1, EmployeeId: 1, Month: 'May', Year: '2010', TotalPresent: '10', TotalAbsent: '1', LeaveDays: '3', Deduction: '2', Leave: '4', FestivalAdvance: '2', HousingLoan: '3', VehicleLoan: '2', OtherLoan: '3', LossOfPay: '4', TDS: '6', ProfessionalFees: '6', OtherDeductions: '4', TotalEarnings: '3', OtherPay: '2',BasicSalary: '3',SalaryPerDay: '4',Pay: '1',Earnings: '3',Deductions: '4',NetPay: '6'},
    { PayrollId: 1, EmployeeId: 9, Month: 'April', Year: '2010', TotalPresent: '10', TotalAbsent: '1', LeaveDays: '3', Deduction: '2', Leave: '4', FestivalAdvance: '2', HousingLoan: '3', VehicleLoan: '2', OtherLoan: '3', LossOfPay: '4', TDS: '6', ProfessionalFees: '6', OtherDeductions: '4', TotalEarnings: '3', OtherPay: '2',BasicSalary: '3',SalaryPerDay: '4',Pay: '1',Earnings: '3',Deductions: '4',NetPay: '6'},
    { PayrollId: 1, EmployeeId: 1, Month: 'March', Year: '2010', TotalPresent: '10', TotalAbsent: '1', LeaveDays: '3', Deduction: '2', Leave: '4', FestivalAdvance: '2', HousingLoan: '3', VehicleLoan: '2', OtherLoan: '3', LossOfPay: '4', TDS: '6', ProfessionalFees: '6', OtherDeductions: '4', TotalEarnings: '3', OtherPay: '2',BasicSalary: '3',SalaryPerDay: '4',Pay: '1',Earnings: '3',Deductions: '4',NetPay: '6'},
    { PayrollId: 1, EmployeeId: 1, Month: 'June', Year: '2010', TotalPresent: '10', TotalAbsent: '1', LeaveDays: '3', Deduction: '2', Leave: '4', FestivalAdvance: '2', HousingLoan: '3', VehicleLoan: '2', OtherLoan: '3', LossOfPay: '4', TDS: '6', ProfessionalFees: '6', OtherDeductions: '4', TotalEarnings: '3', OtherPay: '2',BasicSalary: '3',SalaryPerDay: '4',Pay: '1',Earnings: '3',Deductions: '4',NetPay: '6'},
    { PayrollId: 1, EmployeeId: 1, Month: 'July', Year: '2010', TotalPresent: '10', TotalAbsent: '1', LeaveDays: '3', Deduction: '2', Leave: '4', FestivalAdvance: '2', HousingLoan: '3', VehicleLoan: '2', OtherLoan: '3', LossOfPay: '4', TDS: '6', ProfessionalFees: '6', OtherDeductions: '4', TotalEarnings: '3', OtherPay: '2',BasicSalary: '3',SalaryPerDay: '4',Pay: '1',Earnings: '3',Deductions: '4',NetPay: '6'},
  ];
  addPayroll(addPayroll: Payroll) {
    addPayroll.PayrollId = this._payrollDetail.length + 1;
    this._payrollDetail.push(addPayroll);
  }

  editPayroll(editPayroll: Payroll) {
    const index = this._payrollDetail.findIndex(c => c.PayrollId === editPayroll.PayrollId);
    this._payrollDetail[index] = editPayroll;
    this._payrollDetail.push(editPayroll);
  }
  addOrUpdatePayroll(payroll: Payroll) {
    const existPay: number = this.getPayrollId(payroll.PayrollId);
    if (existPay === -1) {
      //will creates leave
      payroll.PayrollId = this._payrollDetail.length + 1;
      payroll.EmployeeId = this._payrollDetail.length + 1;
      this._payrollDetail.push(payroll);
    } else {
      //will updates leave
      this.deletePayroll(payroll.PayrollId);
      this._payrollDetail.push(payroll);
    }
  }

  deletePayroll(id: number) {
    const delPayroll = this.getPayrollId(id);
    this._payrollDetail.splice(delPayroll, 1);
  }

  getPayroll() {
    return this._payrollDetail;
  }
  getPayrollId(payrollId: number): number {
    return this._payrollDetail.findIndex(x => x.PayrollId === payrollId);
  }
}