import { Component, OnInit, Inject } from '@angular/core';
import { Payroll } from './models/payroll';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { PayrollService } from './services/payroll.service';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-payroll',
  templateUrl: './payroll.component.html',
  styleUrls: ['./payroll.component.css']
})
export class PayrollComponent implements OnInit {
  payForm: FormGroup;
  constructor(private fb: FormBuilder, private dialog: MatDialog , private _payService: PayrollService,
    @Inject(MAT_DIALOG_DATA) public data: Payroll){}

  ngOnInit() {
    this.reactiveForm();
  }
  reactiveForm() {
    this.payForm = this.fb.group({
      PayrollId: [this.data.PayrollId, ''],
      EmployeeId: [this.data.EmployeeId, ''],
      Month: [this.data.Month, ''],
      Year: [this.data.Year, ''],
      TotalPresent: [this.data.TotalPresent, ''],
      TotalAbsent: [this.data.TotalAbsent, ''],
      LeaveDays: [this.data.LeaveDays, ''],
      Deduction: [this.data.Deduction, ''],
      Leave: [this.data.Leave, ''],
      FestivalAdvance: [this.data.FestivalAdvance, ''],
      HousingLoan: [this.data.HousingLoan, ''],
      VehicleLoan: [this.data.VehicleLoan, ''],
      OtherLoan: [this.data.OtherLoan, ''],
      LossOfPay: [this.data.LossOfPay, ''],
      TDS: [this.data.TDS, ''],
      ProfessionalFees: [this.data.ProfessionalFees, ''],
      OtherDeductions: [this.data.OtherDeductions, ''],
      TotalEarnings: [this.data.TotalEarnings, ''],
      OtherPay: [this.data.OtherPay, ''],
      BasicSalary: [this.data.BasicSalary, ''],
      SalaryPerDay: [this.data.SalaryPerDay, ''],
      Pay: [this.data.Pay, ''],
      Earnings: [this.data.Earnings, ''],
      Deductions: [this.data.Deductions, ''],
      NetPay: [this.data.NetPay, '']

    });
  }
  public confirmAdd(): void {
    this.data = this.payForm.value;
    this._payService.addOrUpdatePayroll(this.data);
  }
}
