import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MatTableDataSource, MatDialogConfig } from '@angular/material';
import { PayrollService } from '../services/payroll.service';
import { PayrollComponent } from '../payroll.component';
import { Payroll } from '../models/payroll';
import * as jspdf from 'jspdf';
@Component({
  selector: 'app-payroll-list',
  templateUrl: './payroll-list.component.html',
  styleUrls: ['./payroll-list.component.css']
})
export class PayrollListComponent implements OnInit {

  isPopupOpened: true;
  payrollList:Payroll [] = [];
  searchText;
  payrollFullList:Payroll [] =  [];
  @ViewChild('pdfTable', {static: false}) pdfTable: ElementRef;
  constructor(private dialog: MatDialog , private _payrollService: PayrollService) { }

  ngOnInit() {
   this.payrollList = this._payrollService.getPayroll();
   this.payrollFullList = this.payrollList;
  }
  search(){
    if (this.searchText !=""){
      this.payrollFullList = this.payrollFullList.filter(res =>{ 
        return res.Month.toLocaleLowerCase().match(this.searchText.toLocaleLowerCase())
       });
  } else if (this.searchText ==""){
    this.ngOnInit();
    }
  }
  addPayroll() {
    const dialogConfig = new MatDialogConfig();
    this.isPopupOpened = true;
    const dialogRef = this.dialog.open(PayrollComponent, {
      data: {}
    });


    dialogRef.afterClosed().subscribe(result => {
    });
    //dialogConfig.width = '60px';
   
  }

  editPayroll(id: number) {
    this.isPopupOpened = true;
    const editPayrollTest = this._payrollService.getPayroll().find(c => c.PayrollId === id);
    const dialogRef = this.dialog.open(PayrollComponent, {
      data: editPayrollTest
    });


    dialogRef.afterClosed().subscribe(result => {
    });
  }

  deletePayroll(id: number) {
    this._payrollService.deletePayroll(id);
  }

  public openPDF():void {
    let DATA = this.pdfTable.nativeElement;
    let doc = new jspdf('p','pt', 'a4');
    doc.fromHTML(DATA.innerHTML,15,15);
    doc.output('dataurlnewwindow');
  }


  public downloadPDF():void {
    let DATA = this.pdfTable.nativeElement;
    let doc = new jspdf('p','pt', 'a4');

    let handleElement = {
      '#editor':function(element,renderer){
        return true;
      }
    };
    doc.fromHTML(DATA.innerHTML,15,15,{
      'width': 200,
      'elementHandlers': handleElement
    });

    doc.save('angular-demo.pdf');
  }

}
