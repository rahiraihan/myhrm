import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { EmployeeService } from './services/employee.service';
import { Employee } from './models/employee';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  empForm: FormGroup;
  constructor(private fb: FormBuilder, private dialog: MatDialog , private _empService: EmployeeService,
    @Inject(MAT_DIALOG_DATA) public data: Employee){}

    ngOnInit() {
      this.reactiveForm();
    }
  reactiveForm() {
    this.empForm = this.fb.group({
      EmployeeId: [this.data.EmployeeId, ''],
      EmployeeFirstName: [this.data.EmployeeFirstName, ''],
      EmployeeLastName: [this.data.EmployeeLastName, ''],
      EmployeeFullName: [this.data.EmployeeFullName, ''],
      EmployeeFatherName: [this.data.EmployeeFatherName, ''],
      EmployeeDateOfBirth: [this.data.EmployeeDateOfBirth, '']  ,
      EmployeeSex: [this.data.EmployeeSex, ''],
      EmployeeReligion: [this.data.EmployeeReligion, ''],
      EmployeeMaritalStatus: [this.data.EmployeeMaritalStatus, ''],
      EmployeeNationality: [this.data.EmployeeNationality, ''],
      EmployeeHouse: [this.data.EmployeeHouse, '']  ,
      EmployeeStreet: [this.data.EmployeeStreet, ''] ,
      EmployeeCity: [this.data.EmployeeCity, '']  ,
      EmployeeState: [this.data.EmployeeState, ''],
      EmployeePIN: [this.data.EmployeePIN, ''],
      EmployeePhoneNumber: [this.data.EmployeePhoneNumber, ''],
      EmployeeMobileNumber: [this.data.EmployeeMobileNumber, ''],
      EmployeeImage: [this.data.EmployeeImage, ''],
      EmployeeEmail: [this.data.EmployeeEmail, ''],
      EmployeeDateOfJoin: [this.data.EmployeeDateOfJoin, ''],
      EmployeeDateOfConfirm: [this.data.EmployeeDateOfConfirm, ''],
      EmployeeDepartment: [this.data.EmployeeDepartment, ''],
      EmployeeDesignation: [this.data.EmployeeDesignation, ''],
      EmployeeBasicSalary: [this.data.EmployeeBasicSalary, ''],
      EmployeeSalaryPerDay: [this.data.EmployeeSalaryPerDay, ''],
    });
  }

  public confirmAdd(): void {
    this.data = this.empForm.value;
    this._empService.addOrUpdateEmployee(this.data);
  }

}
