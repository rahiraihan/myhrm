import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';
import { Employee } from '../models/employee';


@Injectable({
  providedIn: 'root'
})
export class EmployeeService{
  constructor(private httpClient: HttpClient, private httpService: HttpClient) { }
 
  _employeeDetails: Employee[] = [];
 _employeeDetail: Employee[] = [
    {EmployeeId: 1, EmployeeFirstName: 'admin', EmployeeLastName: 'admin', EmployeeFullName: 'Male',EmployeeFatherName:'Admin',EmployeeDateOfBirth:'11/12/2000',EmployeeSex:'Male',EmployeeReligion:'Male',EmployeeMaritalStatus:'Male',EmployeeNationality:'Male',EmployeeHouse:'Male',EmployeeStreet: '01',EmployeeCity: 'Dhaka',EmployeeState: 'Dhaka', EmployeePIN: '01', EmployeePhoneNumber: 'string',EmployeeMobileNumber: 'string',EmployeeImage: 'string',EmployeeEmail: 'string',EmployeeDateOfJoin: 'Date',EmployeeDateOfConfirm: 'string',EmployeeDepartment: 'string',EmployeeDesignation: 'string',EmployeeBasicSalary: 11,EmployeeSalaryPerDay: 11},
    {EmployeeId: 1, EmployeeFirstName: 'admin', EmployeeLastName: 'admin', EmployeeFullName: 'Female',EmployeeFatherName:'Admin',EmployeeDateOfBirth:'11/12/2000',EmployeeSex:'Male',EmployeeReligion:'Male',EmployeeMaritalStatus:'Male',EmployeeNationality:'Male',EmployeeHouse:'Male',EmployeeStreet: '01',EmployeeCity: 'Dhaka',EmployeeState: 'Dhaka', EmployeePIN: '01', EmployeePhoneNumber: 'string',EmployeeMobileNumber: 'string',EmployeeImage: 'string',EmployeeEmail: 'string',EmployeeDateOfJoin: 'Date',EmployeeDateOfConfirm: 'string',EmployeeDepartment: 'string',EmployeeDesignation: 'string',EmployeeBasicSalary: 11,EmployeeSalaryPerDay: 11},
    {EmployeeId: 1, EmployeeFirstName: 'admin', EmployeeLastName: 'admin', EmployeeFullName: 'Male',EmployeeFatherName:'Admin',EmployeeDateOfBirth:'11/12/2000',EmployeeSex:'Male',EmployeeReligion:'Male',EmployeeMaritalStatus:'Male',EmployeeNationality:'Male',EmployeeHouse:'Male',EmployeeStreet: '01',EmployeeCity: 'Dhaka',EmployeeState: 'Dhaka', EmployeePIN: '01', EmployeePhoneNumber: 'string',EmployeeMobileNumber: 'string',EmployeeImage: 'string',EmployeeEmail: 'string',EmployeeDateOfJoin: 'Date',EmployeeDateOfConfirm: 'string',EmployeeDepartment: 'string',EmployeeDesignation: 'string',EmployeeBasicSalary: 11,EmployeeSalaryPerDay: 11},
    {EmployeeId: 1, EmployeeFirstName: 'admin', EmployeeLastName: 'admin', EmployeeFullName: 'Male',EmployeeFatherName:'Admin',EmployeeDateOfBirth:'11/12/2000',EmployeeSex:'Male',EmployeeReligion:'Male',EmployeeMaritalStatus:'Male',EmployeeNationality:'Male',EmployeeHouse:'Male',EmployeeStreet: '01',EmployeeCity: 'Dhaka',EmployeeState: 'Dhaka', EmployeePIN: '01', EmployeePhoneNumber: 'string',EmployeeMobileNumber: 'string',EmployeeImage: 'string',EmployeeEmail: 'string',EmployeeDateOfJoin: 'Date',EmployeeDateOfConfirm: 'string',EmployeeDepartment: 'string',EmployeeDesignation: 'string',EmployeeBasicSalary: 11,EmployeeSalaryPerDay: 11},
    {EmployeeId: 1, EmployeeFirstName: 'admin', EmployeeLastName: 'admin', EmployeeFullName: 'Male',EmployeeFatherName:'Admin',EmployeeDateOfBirth:'11/12/2000',EmployeeSex:'Male',EmployeeReligion:'Male',EmployeeMaritalStatus:'Male',EmployeeNationality:'Male',EmployeeHouse:'Male',EmployeeStreet: '01',EmployeeCity: 'Dhaka',EmployeeState: 'Dhaka', EmployeePIN: '01', EmployeePhoneNumber: 'string',EmployeeMobileNumber: 'string',EmployeeImage: 'string',EmployeeEmail: 'string',EmployeeDateOfJoin: 'Date',EmployeeDateOfConfirm: 'string',EmployeeDepartment: 'string',EmployeeDesignation: 'string',EmployeeBasicSalary: 11,EmployeeSalaryPerDay: 11}, 
  ];
  addEmployee(addEmployee: Employee) {
    addEmployee.EmployeeId = this._employeeDetail.length + 1;
    this._employeeDetail.push(addEmployee);
  }

  editEmployee(editEmployee: Employee) {
    const index = this._employeeDetail.findIndex(c => c.EmployeeId === editEmployee.EmployeeId);
    this._employeeDetail[index] = editEmployee;
    this._employeeDetail.push(editEmployee);
  }

  deleteEmployee(id: number) {
    const delEmployee = this.getEmployeeId(id);
    this._employeeDetail.splice(delEmployee, 1);
  }

  getEmployee() {
    return this._employeeDetail;
  }

  addOrUpdateEmployee(isEmp: Employee) {
    const existEmp: number = this.getEmployeeId(isEmp.EmployeeId);
    if (existEmp === -1) {
      //will creates leave
      isEmp.EmployeeId = this._employeeDetail.length + 1;
      this._employeeDetail.push(isEmp);
    } else {
      //will updates leave
      this.deleteEmployee(isEmp.EmployeeId);
      this._employeeDetail.push(isEmp);
    }
  }
  getEmployeeId(employeeId: number): number {
    return this._employeeDetail.findIndex(x => x.EmployeeId === employeeId);
  }
}