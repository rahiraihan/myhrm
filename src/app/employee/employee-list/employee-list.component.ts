import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog, MatDialogConfig  } from '@angular/material';
import { EmployeeComponent } from '../employee.component';
import { Employee } from '../models/employee';
import * as jspdf from 'jspdf'; 

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  isPopupOpened: true;
  
  employeeList:Employee[] = [];
  searchText;
  employeeFullList:Employee[] =  [];
  @ViewChild('pdfTable', {static: false}) pdfTable: ElementRef;
  constructor(private dialog: MatDialog ,private _employeeService: EmployeeService) { }

  ngOnInit() {
    this.employeeList = this._employeeService.getEmployee();
    this.employeeFullList = this.employeeList;
  }
  // get employeeList() {
  //   return this._employeeService.getEmployee();
  // }

  search(){
  if (this.searchText !=""){
    this.employeeFullList = this.employeeFullList.filter(res =>{ 
      return res.EmployeeFullName.toLocaleLowerCase().match(this.searchText.toLocaleLowerCase())
     });
} else if (this.searchText ==""){
  this.ngOnInit();
  }
}


addEmployee() {
  const dialogConfig = new MatDialogConfig();
  this.isPopupOpened = true;
  const dialogRef = this.dialog.open(EmployeeComponent, {
    data: {}
  });


  dialogRef.afterClosed().subscribe(result => {
  });
  //dialogConfig.width = '60px';
 
}

editEmployee(id: number) {
  this.isPopupOpened = true;
  const editEmployees = this._employeeService.getEmployee().find(c => c.EmployeeId === id);
  const dialogRef = this.dialog.open(EmployeeComponent, {
    data: editEmployees
  });


  dialogRef.afterClosed().subscribe(result => {
   data: editEmployees
  });
}

deleteEmployee(id: number) {
  this._employeeService.deleteEmployee(id);
}

public openPDF():void {
  let DATA = this.pdfTable.nativeElement;
  let doc = new jspdf('p','pt', 'a4');
  doc.fromHTML(DATA.innerHTML,15,15);
  doc.output('dataurlnewwindow');
}


public downloadPDF():void {
  let DATA = this.pdfTable.nativeElement;
  let doc = new jspdf('p','pt', 'a4');

  let handleElement = {
    '#editor':function(element,renderer){
      return true;
    }
  };
  doc.fromHTML(DATA.innerHTML,15,15,{
    'width': 200,
    'elementHandlers': handleElement
  });

  doc.save('angular-demo.pdf');
}
}