import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http'; 
import { Observable, BehaviorSubject } from 'rxjs';
import { Loan } from '../models/loan';



@Injectable({
  providedIn: 'root'
})
export class LoanService{
  constructor(private httpClient: HttpClient, private httpService: HttpClient) { }
  dataChange: BehaviorSubject<Loan[]> = new BehaviorSubject<Loan[]>([]);
  dialogData: any;
  _loanDetails: Loan[] = [];
  _loanDetail: Loan[] = [
    {LoanId: 1, TypeOfLoan: 'A', DateOfApplication: '11/12/2010', ProposedAmount: '11/11/2019',NoOfInstRecovery:'13/11/2019',StartDateOfRecovery:'3',EndDateOfRecovery: 'True',NoOfDays:'Test',InterestRate:'10',InterestAmount:'11' },
    {LoanId: 2, TypeOfLoan: 'B', DateOfApplication: '21/12/2010', ProposedAmount: '11/11/2019',NoOfInstRecovery:'13/11/2019',StartDateOfRecovery:'3',EndDateOfRecovery: 'True',NoOfDays:'Test',InterestRate:'10',InterestAmount:'11' },
    {LoanId: 3, TypeOfLoan: 'C', DateOfApplication: '11/11/2010', ProposedAmount: '11/11/2019',NoOfInstRecovery:'13/11/2019',StartDateOfRecovery:'3',EndDateOfRecovery: 'True',NoOfDays:'Test',InterestRate:'10',InterestAmount:'11' },
    {LoanId: 4, TypeOfLoan: 'D', DateOfApplication: '11/10/2010', ProposedAmount: '11/11/2019',NoOfInstRecovery:'13/11/2019',StartDateOfRecovery:'3',EndDateOfRecovery: 'True',NoOfDays:'Test',InterestRate:'10',InterestAmount:'11' },

  ];

  getDialogData() {
    return this.dialogData;
  }
  addLoan(addLoan: Loan) {
    addLoan.LoanId = this._loanDetail.length + 1;
    this._loanDetail.push(addLoan);
  }
  
  
  addOrUpdateLoan(loan: Loan) {
    const existLoan: number = this.getLoanId(loan.LoanId);
    if (existLoan === -1) {
      //will creates leave
      loan.LoanId = this._loanDetail.length + 1;
      this._loanDetail.push(loan);
    } else {
      //will updates leave
      this.deleteLoan(loan.LoanId);
      this._loanDetail.push(loan);
    }
  }

  editLoan(editLoan: Loan) {
    const index = this._loanDetail.findIndex(c => c.LoanId === editLoan.LoanId);
    this._loanDetail[index] = editLoan;
    this._loanDetail.push(editLoan);
  }

  deleteLoan(id: number) {
    const delLoan = this.getLoanId(id)
    this._loanDetail.splice(delLoan, 1);
  }

  getLoan() {
    return this._loanDetail;
  }
  getLoanId(loanId: number): number {
    return this._loanDetail.findIndex(loan => loan.LoanId === loanId);
  }
}