import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {LoanComponent} from '../loan.component';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {LoanService} from '../services/loan.service';
import {FormBuilder} from '@angular/forms';
import {Loan} from '../models/loan';
import * as jspdf from 'jspdf';

@Component({
  selector: 'app-loan-list',
  templateUrl: './loan-list.component.html',
  styleUrls: ['./loan-list.component.css']
})
export class LoanListComponent implements OnInit {
  isPopupOpened: true;

  searchText;
  loanList: Loan[] = []; //should be loan type
  loanFullList: Loan[] = [];
  @ViewChild('pdfTable', {static: false}) pdfTable: ElementRef;
  constructor(private dialog: MatDialog, private _loanService: LoanService) {
  }

  ngOnInit() {
    this.loanList = this._loanService.getLoan();
    this.loanFullList = this.loanList;
  }

  search() {
    if (this.searchText != '') {
      this.loanFullList = this.loanFullList.filter(res => {
        return res.TypeOfLoan.toLocaleLowerCase().match(this.searchText.toLocaleLowerCase());
      });
    } else if (this.searchText == '') {
      this.ngOnInit();
    }
  }

  addLoan() {
    const dialogConfig = new MatDialogConfig();
    this.isPopupOpened = true;
    const dialogRef = this.dialog.open(LoanComponent, {
      data: {}
    });
    dialogRef.afterClosed().subscribe(result => {
    });
    // dialogConfig.width = '60px';

  }

  editLoan(id: number) {
    this.isPopupOpened = true;
    const loanData = this._loanService.getLoan().find(c => c.LoanId === id);
    const dialogRef = this.dialog.open(LoanComponent, {
      data: loanData   // loan data itself a object
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  deleteLoan(id: number) {
    this._loanService.deleteLoan(id);
  }

  public openPDF():void {
    let DATA = this.pdfTable.nativeElement;
    let doc = new jspdf('p','pt', 'a4');
    doc.fromHTML(DATA.innerHTML,15,15);
    doc.output('dataurlnewwindow');
  }


  public downloadPDF():void {
    let DATA = this.pdfTable.nativeElement;
    let doc = new jspdf('p','pt', 'a4');

    let handleElement = {
      '#editor':function(element,renderer){
        return true;
      }
    };
    doc.fromHTML(DATA.innerHTML,15,15,{
      'width': 200,
      'elementHandlers': handleElement
    });

    doc.save('angular-demo.pdf');
  }
}
