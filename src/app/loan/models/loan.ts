export interface Loan{
   LoanId: number;
   TypeOfLoan:string; 
   DateOfApplication:string; 
   ProposedAmount :string; 
   NoOfInstRecovery:string; 
   StartDateOfRecovery :string; 
   EndDateOfRecovery :string; 
   NoOfDays :string; 
   InterestRate :string; 
   InterestAmount:string; 
   
}