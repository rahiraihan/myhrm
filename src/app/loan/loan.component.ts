import {Component, OnInit, Inject} from '@angular/core';
import {FormControl, Validators, FormGroup, FormBuilder} from '@angular/forms';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material';
import {LoanService} from './services/loan.service';
import {Loan} from './models/loan';

@Component({
  selector: 'app-loan',
  templateUrl: './loan.component.html',
  styleUrls: ['./loan.component.css']
})
export class LoanComponent implements OnInit {

  loanForm: FormGroup;

  constructor(private fb: FormBuilder, private dialog: MatDialog, private _loanService: LoanService,
              @Inject(MAT_DIALOG_DATA) public data: Loan) {
  }

  ngOnInit() {
    this.reactiveForm();
  }

  reactiveForm() {
    this.loanForm = this.fb.group({
      LoanId: [this.data.LoanId, ''],
      TypeOfLoan: [this.data.TypeOfLoan, ''],
      DateOfApplication: [this.data.DateOfApplication, ''],
      ProposedAmount: [this.data.ProposedAmount, ''],
      NoOfInstRecovery: [this.data.NoOfInstRecovery, ''],
      StartDateOfRecovery: [this.data.StartDateOfRecovery, ''],
      EndDateOfRecovery: [this.data.EndDateOfRecovery, ''],
      NoOfDays: [this.data.NoOfDays, ''],
      InterestRate: [this.data.InterestRate, ''],
      InterestAmount: [this.data.InterestAmount, '']

    });
  }

  public confirmAdd(): void {
    this.data = this.loanForm.value;
    this._loanService.addOrUpdateLoan(this.data);
  }
}
