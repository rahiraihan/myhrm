import { Component, OnInit, ViewChild, ElementRef, QueryList } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { TrainingService } from '../services/training.service';
import { TrainingComponent } from '../training.component';
import { Training } from '../models/training';
import * as jspdf from 'jspdf';  

@Component({
  selector: 'app-training-list',
  templateUrl: './training-list.component.html',
  styleUrls: ['./training-list.component.css']
})
export class TrainingListComponent implements OnInit {
  isPopupOpened: true;
  trainingList : Training []= [];
  searchText;
  trainingFullList : Training []=  [];

  @ViewChild('pdfTable', {static: false}) pdfTable: ElementRef;
  constructor(private dialog: MatDialog , private _trainService: TrainingService) { }

  ngOnInit() {
    this.trainingList = this._trainService.getTraining();
    this.trainingFullList = this.trainingList;
    console.log(this.pdfTable);
  }

  search(){
    if (this.searchText !=""){
      this.trainingFullList = this.trainingFullList.filter(res =>{ 
        return res.TrainingProvider.toLocaleLowerCase().match(this.searchText.toLocaleLowerCase())
       });
  } else if (this.searchText ==""){
    this.ngOnInit();
    }
  }
  addTraining() {
    const dialogConfig = new MatDialogConfig();
    this.isPopupOpened = true;
    const dialogRef = this.dialog.open(TrainingComponent, {
      data: {}
    });


    dialogRef.afterClosed().subscribe(result => {
    });
    //dialogConfig.width = '60px';
   
  }

  editTraining(id: number) {
    this.isPopupOpened = true;
    const training = this._trainService.getTraining().find(c => c.TrainingId === id);
    const dialogRef = this.dialog.open(TrainingComponent, {
      data: training
      
    });


    dialogRef.afterClosed().subscribe(result => {
    });
  }

  deleteTraining(id: number) {
    this._trainService.deleteTraining(id);
  }

  public openPDF():void {
    let DATA = this.pdfTable.nativeElement;
    let doc = new jspdf('p','pt', 'a4');
    doc.fromHTML(DATA.innerHTML,15,15);
    doc.output('dataurlnewwindow');
  }


  public downloadPDF():void {
    let DATA = this.pdfTable.nativeElement;
    let doc = new jspdf('p','pt', 'a4');

    let handleElement = {
      '#editor':function(element,renderer){
        return true;
      }
    };
    doc.fromHTML(DATA.innerHTML,15,15,{
      'width': 200,
      'elementHandlers': handleElement
    });

    doc.save('angular-demo.pdf');
  }

}
