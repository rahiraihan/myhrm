import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Training } from '../models/training';



@Injectable({
  providedIn: 'root'
})
export class TrainingService{
  constructor(private _httpClientService: HttpClient) { }
  dialogData: any;
  _trainingDetails: Training[] = [];
  _trainingDetail: Training[] = [
    {TrainingId: 1, TrainingProvider: 'Rahi', Trainee: 'SS', TrainingLocation: 'Dhaka',StartDate:'13/11/2019',EndDate:'13/11/2019',Status: 'Done' },
    {TrainingId: 2, TrainingProvider: 'Ovi', Trainee: 'RR', TrainingLocation: 'Mirpur',StartDate:'4/11/2019',EndDate:'13/11/2019',Status: 'Not Done' },
    {TrainingId: 3, TrainingProvider: 'Shaon', Trainee: 'NN', TrainingLocation: 'Delhi',StartDate:'15/11/2019',EndDate:'13/11/2019',Status: 'Done' },
    {TrainingId: 4, TrainingProvider: 'Ashik', Trainee: 'KK', TrainingLocation: 'Goran',StartDate:'17/11/2019',EndDate:'13/11/2019',Status: 'Pending' },
    {TrainingId: 5, TrainingProvider: 'Ala Police', Trainee: 'LL', TrainingLocation: 'Kerala',StartDate:'19/11/2019',EndDate:'13/11/2019',Status: 'Ongoing' },

  ];

  getDialogData() {
    return this.dialogData;
  }
  addTraining(addTrain: Training) {
    addTrain.TrainingId = this._trainingDetail.length + 1;
    this._trainingDetail.push(addTrain);
  }

  editTraining(editTraining: Training) {
    const index = this._trainingDetail.findIndex(c => c.TrainingId === editTraining.TrainingId);
    this._trainingDetail[index] = editTraining;
    this._trainingDetail.push(editTraining);
  }

  addOrUpdateTraining(train: Training) {
    const existTrain: number = this.getTrainId(train.TrainingId);
    if (existTrain === -1) {
      //will creates leave
      train.TrainingId = this._trainingDetail.length + 1;
      this._trainingDetail.push(train);
    } else {
      //will updates leave
      this.deleteTraining(train.TrainingId);
      this._trainingDetail.push(train);
    }
  }
  getTrainId(trainingId: number): number {
    return this._trainingDetail.findIndex(x => x.TrainingId === trainingId);
  }
  deleteTraining(id: number) {
    const delTraining = this.getTrainId(id);
    this._trainingDetail.splice(delTraining, 1);
  }

  getTraining() {
    return this._trainingDetail;
  }
}