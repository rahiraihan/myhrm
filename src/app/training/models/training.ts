export interface Training{
    TrainingId: number;
    TrainingProvider:string; 
    Trainee:string; 
    TrainingLocation:string; 
    StartDate:string; 
    EndDate:string; 
    Status:string
}