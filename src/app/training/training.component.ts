import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { TrainingService } from './services/training.service';
import { Training } from './models/training';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css']
})
export class TrainingComponent implements OnInit {
  trainForm: FormGroup;
  constructor(private fb: FormBuilder, private dialog: MatDialog , private _trainService: TrainingService,
    @Inject(MAT_DIALOG_DATA) public data: Training){}

    ngOnInit() {
      this.reactiveForm();
    }
    reactiveForm() {
      this.trainForm = this.fb.group({
        TrainingId: [this.data.TrainingId, ''],
        TrainingProvider: [this.data.TrainingProvider, ''],
        Trainee: [this.data.Trainee, ''],
        TrainingLocation: [this.data.TrainingLocation, ''],
        StartDate: [this.data.StartDate, ''],
        EndDate: [this.data.EndDate, '']  ,
        Status: [this.data.Status, '']  
      });
    }
  public confirmAdd(): void {
    this.data = this.trainForm.value; 
    this._trainService.addOrUpdateTraining(this.data);
  }
}
